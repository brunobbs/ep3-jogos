return {
  position = {},
  movement = {},
  body = {
    size = 32
  },
  control = {
    acceleration = 300.0,
    max_speed = 1000.0
  },
  field = {
    strength = 24
  },
  charge = {
    strength = 16
  }
}
