-- App Config
local gconfig = require("app_config")

-- Required Systems
local RenderingSystem = require("systems/sys_rendering")

local JuicynessSubsystem = require("systems/subsys_juicyness")

-- Global context
local context = require("context")

-- Debugging requirements
local drawDebugInfo = require("debugging/debug_info")
local drawDebugUI = require("debugging/debug_ui")

-- Drawing subroutine
function love.draw()
    local entities = context.entities

    love.graphics.clear()

    -- Run the camera transformation
    love.graphics.push()

    context.camera:transform()

    -- World-bound object drawing (World, objects in the world)

    -- World border
    love.graphics.setColor(0, 0, 1, 0.9)
    love.graphics.circle("line", 0, 0, 1000)

    -- Draw each system world objects (Physics and Input systems use it for debugging)
    RenderingSystem.render(entities)

    JuicynessSubsystem.render(entities)

    if gconfig.debug then drawDebugInfo() end

    -- End World-bound object drawing

    -- Restore transformations before camera transform
    love.graphics.pop()

    -- Camera-bound object drawing (UI, debugging info, etc)
    if gconfig.debug then drawDebugUI() end

    -- End Camera-bound object drawing
end

-- Handle window resize
function love.resize(w, h) context.camera:setViewport(w, h) end
