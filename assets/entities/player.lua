return {
  position = {},
  movement = {},
  control = {
    acceleration = 300.0,
    max_speed = 1000.0
  },
  field = {
    strength = 24
  }
}
