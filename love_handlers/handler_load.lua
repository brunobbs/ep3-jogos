-- App Config
local gconfig = require("app_config")

-- Utils
local Entity = require("common/entity")
local Camera = require("common/camera")

-- Required Systems
local ControlSystem = require("systems/sys_control")
local JuicynessSubsystem = require("systems/subsys_juicyness")

-- Global context
local context = require("context")

-- Load resources
function love.load()
    -- Create camera
    context.camera = Camera:new()
    context.camera:setCenter(0, 0)

    -- Send camera to input system for tracking
    ControlSystem.camera = context.camera

    -- Load monospace font
    love.graphics.setNewFont(gconfig.fontFile)

    -- Create entities
    local scene = love.filesystem.load(gconfig.sceneFile)

    for _, config in ipairs(scene()) do
        local entityName = config.entity
        local amount = config.n

        for index = 1, amount do
            table.insert(context.entities, Entity:new(entityName))

            -- Follow last controllable entity
            if context.entities[#context.entities].props.control then
                ControlSystem.followEntity(context.entities[#context.entities])
                context.following = index
            end
        end
    end

    -- Set Viewport Size
    local width, height = love.graphics.getDimensions()
    context.camera:setViewport(width, height)

    JuicynessSubsystem.initialize(context.camera)
end
