local ControlSystem = require("systems/sys_control")
local context = require("context")

local function drawDebugInfoOne(entity)
    local props = entity.props

    -- Draw movement speed line
    if props.position and props.movement then
        local px = props.position.point.x
        local py = props.position.point.y
        local sx = props.movement.motion.x
        local sy = props.movement.motion.y

        love.graphics.setColor(0, 1, 1)
        love.graphics.line(px, py, px + sx, py + sy)
    end

    love.graphics.setColor(1, 0, 0)
    for _, location in pairs(context.clicked or {}) do
        love.graphics.circle("fill", location.x, location.y, 5)
    end
end

local function drawDebugInfo()
    local entities = context.entities

    for _, entity in ipairs(entities) do
        drawDebugInfoOne(entity)
    end

    if ControlSystem.follow and ControlSystem.follow.props.position then
        local props = ControlSystem.follow.props
        love.graphics.setColor(1, 0, 1)
        love.graphics.circle(
            "line",
            props.position.point.x,
            props.position.point.y,
            ((props.body and props.body.size) or 8) + 5
        )
    end
end

return drawDebugInfo
