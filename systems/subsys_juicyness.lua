local gconfig = require("app_config")
local EventQueue = require("systems/event_queue")
local Vec = require("common/vec")
local ControlSystem = require("systems/sys_control")

local JuicynessSubsystem = {}

----------------------------
-- Helper functions
----------------------------
local function collisionSound()
    love.audio.pause(JuicynessSubsystem.colSound)
    love.audio.play(JuicynessSubsystem.colSound)
end

local function thrustSound(active)
    if active and not JuicynessSubsystem.thrSound:isPlaying() then
        love.audio.play(JuicynessSubsystem.thrSound)
    elseif (not active) and JuicynessSubsystem.thrSound:isPlaying() then
        love.audio.stop(JuicynessSubsystem.thrSound)
    end
end

local function collisionParticles(ent1, ent2, loc)
    local result_v = (ent1.props.movement.motion + ((ent2.props.movement and ent2.props.movement.motion) or Vec())) / 2

    local angle = -math.atan2(result_v.x, result_v.y)

    JuicynessSubsystem.colParticles:setPosition(loc.x, loc.y)
    JuicynessSubsystem.colParticles:setDirection(angle + math.pi / 2)
    JuicynessSubsystem.colParticles:emit(math.floor(result_v:length() / 2))
end

local function thrustParticles(dir)
    JuicynessSubsystem.thrParticles:setPosition(
        JuicynessSubsystem.camera.center.x - 7 * dir.x,
        JuicynessSubsystem.camera.center.y - 7 * dir.y
    )
    local angle = -math.atan2(dir.x, dir.y)
    JuicynessSubsystem.thrParticles:setDirection(angle - math.pi / 2)
    JuicynessSubsystem.thrParticles:emit(100)
end

local function setupColParticles()
    local pSystem = love.graphics.newParticleSystem(love.graphics.newImage("assets/particle.png"), 10000)

    pSystem:setColors(1, 1, 1, 1, 251 / 255, 255 / 255, 30 / 255, 1, 255 / 255, 55 / 255, 5 / 255, .5)
    pSystem:setParticleLifetime(0.01, 0.5)
    pSystem:setSpinVariation(1)
    pSystem:setSizeVariation(1)
    pSystem:setSpin(10)
    pSystem:setRadialAcceleration(-1000)
    pSystem:setSpread(1)
    pSystem:setSpeed(10, 600)
    pSystem:setSizes(0.1, 0)

    return pSystem
end

local function setupThrParticles()
    local pSystem = love.graphics.newParticleSystem(love.graphics.newImage("assets/particle.png"), 10000)

    pSystem:setColors(1, 1, 1, 1, 10 / 255, 210 / 255, 255 / 255, 1, 10 / 255, 99 / 255, 255 / 255, .5)
    pSystem:setParticleLifetime(0.1, 0.4)
    pSystem:setSpinVariation(1)
    pSystem:setSizeVariation(1)
    pSystem:setSpin(10)
    pSystem:setRadialAcceleration(1)
    pSystem:setSpread(1)
    pSystem:setSpeed(10, 100)
    pSystem:setSizes(0.1, 0)

    return pSystem
end

-----------------------
-- Event callbacks
-----------------------
local function juicyCollision(_, data)
    local ent1 = data.entity1
    local ent2 = data.entity2
    local col = data.collisionPoint
    local spd = data.collisionSpeed

    if spd > 2000 then
        if ControlSystem.follow and (ent1.id == ControlSystem.follow.id or ent2.id == ControlSystem.follow.id) then
            collisionSound(col)
        end
        collisionParticles(ent1, ent2, col, spd)
    end
end

local function juicyThrust(_, data)
    if data.entity and data.source == "input" and data.force:length() > 0.1 then
        thrustParticles(data.force:normalized())
        thrustSound(true)
    else
        thrustSound(false)
    end
end
----------------------------
-- Subsystem functions
----------------------------
function JuicynessSubsystem.process(_, dt)
    JuicynessSubsystem.colParticles:update(dt)
    JuicynessSubsystem.thrParticles:update(dt)
end

function JuicynessSubsystem.render(_)
    love.graphics.push()
    love.graphics.setColor(1, 1, 1)
    love.graphics.draw(JuicynessSubsystem.colParticles, 0, 0)
    love.graphics.draw(JuicynessSubsystem.thrParticles, 0, 0)
    love.graphics.pop()
end

----------------------------
-- Module Initializer
----------------------------
function JuicynessSubsystem.initialize(camera)
    if gconfig.debug then
        print("Initializing Juicyness subsystem....")
    end

    EventQueue.registerListener({"PHYS_COLLISION"}, juicyCollision)
    EventQueue.registerListener({"APPLY_FORCE"}, juicyThrust)

    JuicynessSubsystem.colParticles = setupColParticles()
    JuicynessSubsystem.thrParticles = setupThrParticles()

    JuicynessSubsystem.colSound = love.audio.newSource("assets/collision.wav", "static")
    JuicynessSubsystem.colSound:setVolume(0.1)
    JuicynessSubsystem.thrSound = love.audio.newSource("assets/thruster.wav", "static")
    JuicynessSubsystem.thrSound:setLooping(true)
    JuicynessSubsystem.bgm = love.audio.newSource("assets/BGM.mp3", "stream")
    JuicynessSubsystem.bgm:setLooping(true)
    JuicynessSubsystem.bgm:setVolume(0.1)
    JuicynessSubsystem.bgm:play()

    JuicynessSubsystem.camera = camera

    if gconfig.debug then
        print("Juicyness subsystem status: JUICY")
    end
end

return JuicynessSubsystem
