-- luacheck: globals love
local Vec = require("common/vec")

local Camera = {}

function Camera:new(w, h)
    local camera = {
        center = Vec(),
        viewport = Vec(w or 640, h or 480),
        angle = 0,
        zoom_level = 0
    }
    setmetatable(camera, self)

    Camera.__index = self

    return camera
end

function Camera:calculateTransform()
    local transform = love.math.newTransform()

    transform:translate(self.viewport.x / 2, self.viewport.y / 2)
    transform:scale(math.exp(self.zoom_level))
    transform:rotate(self.angle)
    transform:translate(-self.center.x, -self.center.y)

    return transform
end

function Camera:cameraTransform()
    love.graphics.replaceTransform(self:calculateTransform())
end

function Camera:transform()
    self:cameraTransform()
end

function Camera:draw(drawable, transformation)
    love.graphics.push()

    transformation = transformation or love.math.newTransform()

    self:cameraTransform()

    love.graphics.draw(drawable, transformation)

    love.graphics.pop()
end

function Camera:drawq(texture, quad, transformation)
    love.graphics.push()

    transformation = transformation or love.math.newTransform()

    self:cameraTransform()

    love.graphics.draw(texture, quad, transformation)

    love.graphics.pop()
end

function Camera:setCenter(x, y)
    self.center = Vec(x, y)
end

function Camera:setViewport(w, h)
    self.viewport = Vec(w, h)
end

function Camera:setAngle(a)
    self.angle = a or 0
end

function Camera:setZoom(z)
    self.zoom_level = z or 0
end

function Camera:translate(x, y)
    self.center = self.center + Vec(x, y)
end

function Camera:rotate(a)
    self.angle = self.angle + a
end

function Camera:zoom(z)
    self.zoom_level = self.zoom_level + z
end

function Camera:pixelToCoord(x, y)
    return self:calculateTransform():inverseTransformPoint(x, y)
end

function Camera:coordToPixel(x, y)
    return self:calculateTransform():transformPoint(x, y)
end

return Camera
