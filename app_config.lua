return {
    debug = false,
    seed = nil,
    fontFile = "assets/fonts/droid_sans_mono.ttf",
    sceneFile = "assets/scenes/test.lua"
}
