-- luacheck: globals love
local util = require("common/util")
local Vec = require("common/vec")

local Entity = {
    nid = 0,
    default = {
        position = {point = Vec(0, 0)},
        movement = {motion = Vec(0, 0)},
        body = {size = 8},
        control = {acceleration = 0.0, max_speed = 50.0},
        field = {strength = 1},
        charge = {strength = 1}
    },
    cachedConfig = {}
}

function Entity:new(configName)
    local entity = {id = Entity.nid, type = configName, props = {}}

    Entity.nid = Entity.nid + 1

    setmetatable(entity, self)

    -- Cache
    if not self.cachedConfig[configName] then
        self.cachedConfig[configName] = love.filesystem.load(
                                            "assets/entities/" .. configName ..
                                                ".lua")()
    end

    -- Copy
    entity.props = util.deepcopy(self.cachedConfig[configName])

    -- Default values
    if entity.props.position then
        local size = (entity.props.body and entity.props.body.size) or 8
        local x
        local y

        repeat
            x = math.random(size - 1000, 1000 - size)
            y = math.random(size - 1000, 1000 - size)
        until x ^ 2 + y ^ 2 < (1000 - size) ^ 2

        entity.props.position.point = Vec(x, y)
    end

    for propK, propV in pairs(entity.props) do
        for defK, defV in pairs(Entity.default[propK]) do
            if not propV[defK] then propV[defK] = defV end
        end
    end

    Entity.__index = self

    return entity
end

return Entity
