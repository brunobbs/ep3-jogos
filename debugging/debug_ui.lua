local ControlSystem = require("systems/sys_control")

local context = require("context")

local linen = 0

local function vecToString(vec)
    return "(" .. string.format("%.3f", vec.x) .. ", " .. string.format("%.3f", vec.y) .. ")"
end

local function println(text, color)
    color = color or {1, 1, 1}

    love.graphics.setColor(color[1], color[2], color[3])
    love.graphics.print(text, 0, linen * 15)

    linen = linen + 1
end

local function drawDebugUI()
    linen = 0

    local camera = context.camera

    local followed =
        ControlSystem.follow and (tostring(ControlSystem.follow.id) .. " [" .. ControlSystem.follow.type .. "]")

    -- Simulation speed
    println("Simulation Speed: x " .. context.simulationSpeed)

    -- Camera status
    if context.paused then
        println("Camera: [PAUSED] Manual control Fallback", {1, 1, 0})
    elseif followed then
        println("Camera: Following entity: " .. followed, {0, 1, 0})
    else
        println("Camera: On Manual control", {1, 1, 0})
    end

    println(" - Camera.viewport: " .. vecToString(camera.viewport))
    println(" - Camera.center:   " .. vecToString(camera.center))
    println(" - Camera.angle:    " .. tostring(camera.angle))
    println(" - Camera.zoom:     " .. tostring(camera.zoom_level))

    -- Followed entity properties
    println("")

    if followed then
        local props = ControlSystem.follow.props

        println("Followed Entity: " .. followed, {0, 1, 0})

        if props.position then
            println(" - Entity.position: " .. vecToString(props.position.point))
        end

        if props.movement then
            println(
                " - Entity.movement: " ..
                    vecToString(props.movement.motion) ..
                        " [" .. string.format("%.3f", props.movement.motion:length()) .. "]"
            )
        end

        if props.body then
            println(" - Entity.body   : " .. props.body.size)
        end

        if props.control then
            println(" - Entity.control : Can control!", {0, 1, 0})
            println("   - acceleration = " .. props.control.acceleration)
            println("   - max_speed    = " .. props.control.max_speed)
        else
            println(" - Entity.control : Can't control", {1, 0, 0})
        end

        if props.field then
            println(" - Entity.field   : " .. props.field.strength)
        end

        if props.charge then
            println(" - Entity.charge  : " .. props.charge.strength)
        end
    else
        println("Not following any entity", {1, 0, 0})
    end
end

return drawDebugUI
