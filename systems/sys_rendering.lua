local EventQueue = require("systems/event_queue")

local RenderingSystem = {timer = 0, inputForces = {}}

local colors = {r = {1, 0.2, 0.2}, g = {0.2, 1, 0.2}, b = {0.2, 0.2, 1}}

local function inputForceListener(_, data)
    if data.entity and data.source == "input" then
        RenderingSystem.inputForces[data.entity.id] = data.force
    end
end

EventQueue.registerListener({"APPLY_FORCE"}, inputForceListener)

function RenderingSystem.process(_, dt)
    RenderingSystem.timer = RenderingSystem.timer + dt
end

function RenderingSystem.render(entities)
    for _, entity in ipairs(entities) do
        love.graphics.push()
        RenderingSystem.renderOne(entity)
        love.graphics.pop()
    end
end

function RenderingSystem.renderOne(entity)
    local props = entity.props

    -- Position property
    if not props.position then
        return
    end

    local posx = props.position.point.x or 0
    local posy = props.position.point.y or 0

    -- Field property
    if props.field then
        local field = props.field
        local color

        -- Set field color
        if field.strength > 0 then
            color = colors.r
        elseif field.strength < 0 then
            color = colors.b
        else
            color = colors.g
        end

        love.graphics.setColor(color[1], color[2], color[3], 0.7)
        love.graphics.circle("line", posx, posy, math.abs(field.strength))
    end

    -- Body property
    if not props.field then
        love.graphics.setColor(colors.g[1], colors.g[2], colors.g[3], 0.7)
    end

    if props.body then
        love.graphics.circle("fill", posx, posy, props.body.size or 8)
    end

    -- Charge property
    if props.charge then
        local charge = props.charge

        local angle = 2 * math.pi * RenderingSystem.timer * math.sqrt(math.abs(charge.strength))

        local dx = math.cos(angle) * 8
        local dy = math.sin(angle) * 8

        local color

        if charge.strength > 0 then
            color = colors.r
        elseif charge.strength < 0 then
            color = colors.b
        else
            color = colors.g
        end

        love.graphics.setColor(color[1], color[2], color[3], 0.8)
        love.graphics.circle("fill", posx + dx, posy + dy, 4)
        love.graphics.circle("fill", posx - dx, posy - dy, 4)
    end

    -- Renders default circle of the same color of charge
    if not props.charge then
        love.graphics.setColor(0, 1, 0, 0.9)
    end

    if (not props.body) and (not props.field) then
        love.graphics.circle("line", posx, posy, 8)
    end

    -- Control property
    if props.position and props.control then
        love.graphics.setColor(1, 1, 1)

        local inputForce = RenderingSystem.inputForces[entity.id]
        RenderingSystem.inputForces[entity.id] = nil

        if inputForce and inputForce:length() > 0.1 then
            local angles = {0, 2.5, -2.5}
            local vertices = {}

            local angle_offset = math.atan2(inputForce.x, inputForce.y)

            for _, angle in pairs(angles) do
                table.insert(vertices, 8 * math.sin(angle + angle_offset) + props.position.point.x)
                table.insert(vertices, 8 * math.cos(angle + angle_offset) + props.position.point.y)
            end

            love.graphics.polygon("fill", vertices)
        else
            love.graphics.circle("fill", props.position.point.x, props.position.point.y, 8)
        end
    end
end

return RenderingSystem
