# Relatório EP3 - MAC0346

Nome: Bruno Boaventura Scholl
NUSP: 9793586

Nome: Victor Seiji Hariki
NUSP: 9793694

## Arquitetura

A arquitetura foi feita separando os sistemas de:
 - Gráficos (systems/sys_rendering.lua)
 - Física (systems/sys_physics.lua)
 - Controle (systems/sys_control.lua)
 - Juicyness (systems/subsys_juicyness.lua)

E utiliza um broadcast de eventos síncrona (systems/event_queue.lua) para algumas comunicações entre sistemas.

O estado de câmera e entidades é centralizado (context.lua), mas para maior flexibilidade dos sistemas, não é utilizado diretamente neles, com as entidades sendo passadas como um parâmetro.

As entidades são objetos com um parâmetro props, contendo as propriedades da entidade. Cada sistema decide o que irá utilizar e o que irá ignorar. O sistema não é assim um Entity-Component puro.

### Rendering System

Sistema responsável por desenhar as entidades na tela. Como todo sistema, possui um método 'process(entities, dt)', que apenas atualiza o timer interno para a renderização das cargas rotatórias.

Esse sistema escuta o evento de 'APPLY_FORCE' de tipo input, sabendo assim a força sendo aplicada em cada entidade vinda de um evento de controle, permitindo a renderização da seta de direção de cada entidade. (Esse evento é emitido pelo Control System)

Alguns pontos que não seguem à risca a especificação:
 - Não estava claro como desenhar a nave em caso de não haver forças no jogador. O triângulo nesse caso é substituído por um círculo de raio 8px.

### Physics System

Sistema responsável pelo processamento da física entre entidades. Esse sistema faz verificação entre todos os pares de entidades para lidar com colisão.

Em colisão são enviados eventos de colisão, que são usados pelo sistema de Juicyness.

Esse sistema escuta o evento 'APPLY_FORCE', e aplica a força pra entidade referida.

Alguns pontos que não seguem à risca a especificação:
 - Não estava claro como tratar a colisão entre duas entidades em movimento. Utilizei uma fórmula parecida com a entre entidade em movimento e entidade fixa, mas calculando as novas velocidades de modo inversamente proporcional à massa da entidade.
 - Existe um valor collisionLoss que é a perda de energia que ocorre nas colisões. Para manter de acordo com o enunciado, tem um default de 1. Para colisões completamente elásticas, é possível o fazer ser 2.

### Control System

Sistema responsável pelo controle e movimento da câmera para seguir a entidade selecionada. Esse sistema pode ser configurado para seguir qualquer entidade, mas pode controlar apenas entidades com a propriedade 'control'.

Esse sistema interage com o sistema de física enviando eventos de 'APPLY_FORCE'. Esse evento é usado por todos os outros sistemas.

### Juicyness Subsystem

Esse subsistema é responsável pela música, sons e partículas. Ele escuta eventos de colisão e controle e os utiliza para criar partículas e som de colisão e do foguete.

## Arquivos

O código é separado em várias pastas, cada um como uma categoria geral:

### common

Armazena modelos e Classes comuns (Vetores, classes, entidades, funções de utilidade e câmera - Essa última foi reaproveitada do EP anterior)

### debugging

Armazena funções para renderização de informações de debugging. É possível vê-las editando app_config.lua, colocando 'debug = true'

### love_handlers

Armazena handlers do love, como love.setup e love.update. O main estava ficando muito grande, por isso decidimos separá-los.

### systems

Armazena os sistemas, subsistemas, e a event queue.

### ./
Armazena o main, configurações (conf.lua e app_config.lua), e contexto.

## Controles

Há diversos controles disponíveis em nossa implementação. Podemos dividí-los nas seguintes categorias:

### Movimento
Os controles de movimento ficam ativados quando há uma entidade sendo controlada. Quando uma entidade está selecionada a câmera fica centralizada nela. Os controles são os seguintes:
| Comando |  Ação  |
|:-------:|:-------|
|    W    |  Cima  |
|    A    |Esquerda|
|    S    | Baixo  |
|    D    | Direita|

### Câmera
Os controles de câmera são os seguintes:
| Comando | Ação               |
|:-------:|:-------------------|
|    W    | Cima               |
|    A    | Esquerda           |
|    S    | Baixo              |
|    D    | Direita            |
|    Q    | Rotaciona a-horário|
|    E    | Rotaciona horário  |
|    R    | Aproxima o zoom    |
|    F    | Afasta o zoom      |
|    X    | Câmera Livre       |
|  Enter  | Volta o zoom padrão|

### Simulação
A velocidade de simulação pode ser alterada com os seguintes controles:
| Comando | Ação                              |
|:-------:|:----------------------------------|
|    -    | Reduz a velocidade de simulação   |
|    +    | Aumenta a velocidade de simulação |
|  Espaço | Pausa a simulação                 |
|  Enter  | Volta à velocidade padrão         |

### Mudança de entidade controlada
Para mudar de entidade controlada, os controles são os seguintes:
| Comando |  Ação                         |
|:-------:|:------------------------------|
| Clique 1| Seleciona a entidade no cursor|
|   TAB   | Próxima Entidade na lista     |


## Tarefas Realizadas

Nós fizemos as tarefas:

| Código | Tarefa                                                     | Máximo | Feita |
| :----: | :--------------------------------------------------------: | :----: | :---: |
| A1     | Atender o formato de entrega                               | 10     |  [X]  |
| A2     | Executar sem erros                                         | 10     |  [X]  |
| A3     | Relatório completo e adequado                              | 20     |  [X]  |
| A4     | Arquivo [conf.lua][1] apropriado                           | 10     |  [X]  |
| A5     | Cenas e entidades adicionais de teste                      | 10     |  []   |
| S1     | Limitar o espaço de simulação                              | 10     |  [X]  |
| S2     | Propriedade de posição                                     |  5     |  [X]  |
| S3     | Posicionar aleatoriamente entidades sem posição explícita  |  5     |  [X]  |
| S4     | Propriedade de movimento                                   | 10     |  [X]  |
| S5     | Propriedade de corpo                                       | 10     |  [X]  |
| S6     | Propriedade de controle                                    | 10     |  [X]  |
| S7     | Propriedades de campo e carga                              | 20     |  [X]  |
| R1     | Centralizar a tela na origem                               |  5     |  [X]  |
| R2     | Renderizar os limites do espaço de simulação               |  5     |  [X]  |
| R3     | Renderizar a propriedade de posição                        | 10     |  [X]  |
| R4     | Renderizar a propriedade de corpo                          | 10     |  [X]  |
| R5     | Renderizar a propriedade de controle                       | 10     |  [X]  |
| R6     | Centralizar a tela na entidade controla, se houver         | 10     |  [X]  |
| R7     | Renderizar a propriedade de campo                          | 10     |  [X]  |
| R8     | Renderizar a propriedade de carga                          | 10     |  [X]  |
| Q1     | Passar no [luacheck](https://github.com/mpeterv/luacheck)  | 20     |  [X]  |
| Q2     | Organizar em módulos (1)                                   | 10     |  []   |
| Q3     | Organizar em funções (2)                                   | 10     |  []   |
| Q4     | Nomes claros de variáveis, funções e módulos               |  5     |  [X]  |
| E1     | Usar uma arquitetura Entity-Component-System               | 10     |  [X]  |
| E2     | Opção para dar *zoom in* e *out* na câmera                 | 10     |  [X]  |
| E3     | Não posicionar aleatoriamente entidades colidindo (díficil)| 20     |  []   |
| E4     | Opção para controlar a velocidade de simulação             | 10     |  [X]  |
| E5     | Efeitos de *juiciness*                                     | 20     |  [X]  |