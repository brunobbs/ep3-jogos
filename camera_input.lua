-- luacheck: globals love
local context = require("context")

local spd = 500
local aspd = 3.14
local zspd = 5
local sspd = 0.5

local cameraMovement = function(camera, dt)
    if love.keyboard.isDown("w") then camera:translate(0, -spd * dt) end

    if love.keyboard.isDown("s") then camera:translate(0, spd * dt) end

    if love.keyboard.isDown("a") then camera:translate(-spd * dt, 0) end

    if love.keyboard.isDown("d") then camera:translate(spd * dt, 0) end

    if love.keyboard.isDown("q") then camera:rotate(-aspd * dt) end

    if love.keyboard.isDown("e") then camera:rotate(aspd * dt) end

    if love.keyboard.isDown("r") then camera:zoom(zspd * dt) end

    if love.keyboard.isDown("f") then camera:zoom(-zspd * dt) end

    if love.keyboard.isDown("=") then
        context.simulationSpeed = math.min(2, context.simulationSpeed + sspd * dt)
    end

    if love.keyboard.isDown("-") then
        context.simulationSpeed = math.max(0, context.simulationSpeed - sspd * dt)

    end
end

return cameraMovement
