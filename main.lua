-- luacheck: globals love
-- Reads command line options
local gconfig = require("app_config")

if arg[2] then
    if arg[2] == "--debug" then
        gconfig.debug = true
        if arg[3] then
            gconfig.sceneFile = "assets/scenes/" .. arg[3] .. ".lua"
        end
    else
        gconfig.sceneFile = "assets/scenes/" .. arg[2] .. ".lua"
    end
end

-- Seed random
math.randomseed(gconfig.seed or os.time())

-- Love handler modules
require("love_handlers/handler_load")
require("love_handlers/handler_input")
require("love_handlers/handler_graphics")
require("love_handlers/handler_processing")
