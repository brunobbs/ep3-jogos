local EventQueue = require("systems/event_queue")
local Vec = require("common/vec")

local PhysicsSystem = {forces = {}}

local function externalForceListener(_, data)
    table.insert(PhysicsSystem.forces, data)
end

EventQueue.registerListener({"APPLY_FORCE"}, externalForceListener)

function PhysicsSystem.process(entities, dt)
    PhysicsSystem.calculateForces(entities)
    PhysicsSystem.applyForces(entities, dt)
    PhysicsSystem.resolveCollisions(entities, dt)
    PhysicsSystem.applyMovement(entities, dt)
end

function PhysicsSystem.calculateForces(entities)
    for _, entity in pairs(entities) do
        local props = entity.props

        -- Charge
        if props.position and props.charge then
            -- For every other entity that has a field
            for _, otherEntity in pairs(entities) do
                if otherEntity.props.position and otherEntity.props.field and not (otherEntity.id == entity.id) then
                    -- Calculates force to be applied
                    local diffV = props.position.point - otherEntity.props.position.point

                    local fieldMultiplier = 1000 * otherEntity.props.field.strength * props.charge.strength
                    local distanceFraction = diffV / diffV:dot(diffV)

                    -- Insert force
                    table.insert(
                        PhysicsSystem.forces,
                        {
                            entity = entity,
                            source = "charge",
                            force = distanceFraction * fieldMultiplier
                        }
                    )
                end
            end
        end
    end
end

function PhysicsSystem.applyForces(entities, dt)
    for _, force in pairs(PhysicsSystem.forces) do
        local affected = (force.entity and {force.entity}) or entities
        for _, entity in pairs(affected) do
            local props = entity.props

            if props.movement then
                local accel = force.force / ((props.body and props.body.size) or 1)

                props.movement.motion = props.movement.motion + accel * dt
            end
        end
    end

    PhysicsSystem.forces = {}
end

function PhysicsSystem.resolveCollisions(entities, _)
    for _, entity in pairs(entities) do
        local props = entity.props

        -- Colision
        local colisionLoss = 1

        if props.position and props.movement then
            for _, otherEntity in pairs(entities) do
                local otherP = otherEntity.props
                local size = (props.body and props.body.size) or 8

                if otherP.position and otherP.body and not (otherEntity.id == entity.id) then
                    local diffV = otherP.position.point - props.position.point
                    local sqrDist = diffV:dot(diffV)
                    local minDist = otherP.body.size + size

                    -- Check for each other entity if within collision range
                    if sqrDist < minDist ^ 2 then
                        local dist = math.sqrt(sqrDist)
                        local delta = minDist - dist + 0.1
                        local dir = diffV:normalized()

                        local speedDelta =
                            props.movement.motion - ((otherP.movement and otherP.movement.motion) or Vec())

                        -- Send collision notification event
                        EventQueue.sendEvent(
                            "PHYS_COLLISION",
                            {
                                entity1 = entity,
                                entity2 = otherEntity,
                                collisionPoint = otherP.position.point - diffV * (otherP.body.size / minDist),
                                collisionSpeed = math.max(0, diffV:dot(speedDelta))
                            }
                        )

                        -- If both can move, each moves proportionaly to the other
                        if otherP.movement then
                            props.position.point = props.position.point - dir * (otherP.body.size / minDist) * delta
                            props.movement.motion =
                                props.movement.motion -
                                dir * dir:dot(speedDelta) * (otherP.body.size / minDist) * colisionLoss
                            otherP.position.point = otherP.position.point + dir * (size / minDist) * delta
                            otherP.movement.motion =
                                otherP.movement.motion -
                                (-dir) * (-dir):dot(-speedDelta) * (size / minDist) * colisionLoss
                        else
                            props.position.point = props.position.point - dir * delta
                            props.movement.motion =
                                props.movement.motion - dir * dir:dot(props.movement.motion) * colisionLoss
                        end
                    end
                end
            end
        end
    end
end

function PhysicsSystem.applyMovement(entities, dt)
    for _, entity in pairs(entities) do
        local props = entity.props

        -- Movement
        if props.position and props.movement then
            props.position.point = props.position.point + props.movement.motion * dt

            local distFromCenter = math.sqrt(props.position.point.x ^ 2 + props.position.point.y ^ 2)
            local maxDistFromCenter = 1000 - ((props.body and props.body.size) or 8)

            if distFromCenter > maxDistFromCenter then
                props.position.point = -(props.position.point / distFromCenter) * maxDistFromCenter
            end

            if props.control and props.control.max_speed then
                props.movement.motion:clamp(props.control.max_speed)
            end
        end
    end
end

return PhysicsSystem
