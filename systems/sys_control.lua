local gconfig = require("app_config")
local Vec = require("common/vec")
local EventQueue = require("systems/event_queue")

local ControlSystem = {camera = nil, follow = nil}

function ControlSystem.followEntity(entity)
    ControlSystem.follow = entity
end

function ControlSystem.render(entities)
    for _, entity in ipairs(entities) do
        ControlSystem.renderOne(entity)
    end
end

function ControlSystem.renderOne(entity)
    local props = entity.props

    if gconfig.debug then
        if props.control then
            local px = props.position.point.x
            local py = props.position.point.y
            local sx = props.movement.motion.xspace
            local sy = props.movement.motion.y

            love.graphics.setColor(1, 0, 0)
            love.graphics.line(px, py, px + sx, py + sy)
        end
    end
end

function ControlSystem.process(_, dt)
    if ControlSystem.follow and ControlSystem.follow.props.position then
        local pos = ControlSystem.follow.props.position.point
        ControlSystem.camera:setCenter(pos.x, pos.y)
    end

    if ControlSystem.follow then
        ControlSystem.processOne(ControlSystem.follow, dt)
    end
end

function ControlSystem.processOne(entity, _)
    -- Check if has control
    if entity.props.control and entity.props.movement then
        local force = Vec()
        local accel = entity.props.control.acceleration

        if love.keyboard.isDown("d") then
            force.x = force.x + 1
        end
        if love.keyboard.isDown("a") then
            force.x = force.x - 1
        end
        if love.keyboard.isDown("w") then
            force.y = force.y - 1
        end
        if love.keyboard.isDown("s") then
            force.y = force.y + 1
        end

        force = force:normalized()

        -- Send collision notification event
        EventQueue.sendEvent(
            "APPLY_FORCE",
            {
                entity = entity,
                source = "input",
                force = force * accel * ((entity.props.body and entity.props.body.size) or 1)
            }
        )
    end
end

return ControlSystem
