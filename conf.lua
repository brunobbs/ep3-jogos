-- luacheck: globals love
function love.conf(t)
    t.window.width = 800
    t.window.height = 600
    t.window.resizable = true
    t.window.title = "EP3 - Scholl e Seiji"

    -- Disable modules that are not needed
    t.modules.joystick = false
    t.modules.physics = false
    t.modules.touch = false
end
