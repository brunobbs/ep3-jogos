-- Utils
local manualCamera = require("camera_input")

-- Required Systems
local ControlSystem = require("systems/sys_control")
local PhysicsSystem = require("systems/sys_physics")
local RenderingSystem = require("systems/sys_rendering")

local JuicynessSubsystem = require("systems/subsys_juicyness")

-- Global context
local context = require("context")

function love.update(dt)
    local entities = context.entities

    -- Is always run for zoom support
    manualCamera(context.camera, dt)

    dt = dt * context.simulationSpeed

    if context.paused then
        dt = 0
    end

    -- Run Updates for Systems
    ControlSystem.process(entities, dt)
    PhysicsSystem.process(entities, dt)
    RenderingSystem.process(entities, dt)

    JuicynessSubsystem.process(entities, dt)
end
