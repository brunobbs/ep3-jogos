local Vec = require("common/vec")

local context = require("context")

local ControlSystem = require("systems/sys_control")

local function follow(index)
    context.following = index
    ControlSystem.followEntity(index and context.entities[index])
end

function love.keypressed(key)
    if key == "tab" then
        -- TAB key switches entity being followed. Entities other than controllable ones may also be followed
        local newIndex

        if not context.following then
            newIndex = 1
        elseif context.following > #context.entities then
            newIndex = nil
        else
            newIndex = context.following + 1
        end

        follow(newIndex)
    elseif key == "x" then
        -- X key sets camera to manual mode immediately
        follow(nil)
    elseif key == "space" then
        -- SPACE key pauses and unpauses the simulation
        context.paused = not context.paused
    elseif key == "return" then
        -- ENTER key resets simulaton speed, camera zoom and angle the simulation
        context.simulationSpeed = 1
        context.camera:setZoom()
        context.camera:setAngle()
    end
end

function love.mousepressed(x, y, button, _)
    x, y = context.camera:pixelToCoord(x, y)

    if button == 1 then
        -- If entity was clicked, focus on it
        for index, entity in pairs(context.entities) do
            local props = entity.props
            local pos = props.position.point
            local size = ((props.body and props.body.size) or 8)

            if props.position then
                if (pos - Vec(x, y)):length() < size then
                    follow(index)
                    return
                end
            end
        end
    end
end
