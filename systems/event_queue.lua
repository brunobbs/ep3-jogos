local EventQueue = {listeners = {}}

-- PHYS_COLLISION // Advertises entity collisions
-- data: {
--   entity1: entity,
--   enitty2: entity,
--   collisionPoint: Vec
--   collisionSpeed: number
-- }

-- APPLY_FORCE // Applies force to system
-- data: {
--   entity: entity or nil, // nil applies force to all entities
--   source: string, // Origin of the force
--   force: Vec
-- }

function EventQueue.registerListener(types, handler)
    for _, type in pairs(types) do
        if not EventQueue.listeners[type] then
            EventQueue.listeners[type] = {}
        end

        table.insert(EventQueue.listeners[type], handler)
    end
end

function EventQueue.sendEvent(type, payload)
    if not EventQueue.listeners[type] then
        return
    else
        for _, listener in pairs(EventQueue.listeners[type]) do
            listener(type, payload)
        end
    end
end

return EventQueue
